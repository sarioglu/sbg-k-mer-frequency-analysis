# Data Structure
## Kmer
Each k-mer is structured as an array of 2-bit elements which represent 4 nucleotides. There is not any pre-specified length or max-length for these arrays. To achieve this degree of dynamism a `char*` is used. Therefore, each k-mer requires `(4+k/4)` bytes in memory.

## KmerMap
`KmerMap` is an `unordered_map` which holds a `Kmer` and its count. Since performance of counting stricly depends on the performance of finding the corresponding element, `unordered_map` makes a good job. It has constant complexity for both `find` and `insert` methods, which are better that `list` or `vector`, yet is does not store the elements in an order. `map` is also not suitable, since it orders elements based on their keys.

## KmerList
`KmerList` is used to get and ordered and limited-length part of the `KmerMap`. It is choosen to be a `list`, since it has a good performance of `insert` and `pop_back`.

## Bloom Filter
`Kmer` enables to hold k-mers with minimum memory space. But, files can include billions of them. In such scenario, storing every of them violates the memory requirement of this task. Early inspections of me on smaller files showed that nearly 80% of the k-mers present only once. Bloom filter is a good option to store the presence of k-mers and filter out the huge and meaningless part of the file.

Filtering that part may not be enough for greater files. That's why I have applied same structure to eliminate k-mers which repeats for 2 times, which are another big part of the files.

Bloom filters requires number of elements to be inserted while preparing them. I have used file size to calculate them. Roughly, we can assume that 1/2.5 of each file is formed by DNA sequences.

I have used the implementaion of Arash Partow (<http://www.partow.net>) which is distributed under CPL.

It is also possible to use counting bloom filters to store the number of k-mers. However, their impact on memory consumption would be greater than their usefulness.

# Usage
Application is written in C++11 and compatible with Linux x64. It requires OpenMP to work multi-threaded, yet it is not a must.

Use `make` command to compile the code into executable "kmerCounter". The application requires 3 arguments to work with. You may run it like `./kmerCounter -f filename [-k kmerLength] [-c listLength]`. Default values of kmerLength and listLength are set to 30 and 25 respectively. filename is mandatory.

# Performance Evaluation
I have used two files for tests:

1. ERR047698.filt.fastq
    * filesize: 45MB
    * number of k-mers: ~12 million

2. ERR055763_2.filt.fastq
    * filesize: 4.18 GB
    * number of k-mers: ~13 billion

On a regular notebook with Core i7 6700HQ processor, application takes ~10 sec. and ~20 MB to process first file with default values. Second file takes ~21 min. and ~3 GB under same conditions.
