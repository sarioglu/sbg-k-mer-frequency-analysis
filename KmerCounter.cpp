#include "KmerCounter.h"

KmerCounter& KmerCounter::getInstance() {
	static KmerCounter instance;

	return instance;
}

KmerCounter::~KmerCounter() {
	if (this->filter1 != NULL)
		delete this->filter1;
	if (this->filter2 != NULL)
		delete this->filter2;
}

void KmerCounter::init(const uint64_t sizeEstimation) {
	this->createFilters(sizeEstimation);
}

void KmerCounter::createFilters(const uint64_t sizeEstimation) {
	// Create first bloom filter to eliminate one-time occurances
	bloom_parameters parameters1;
	parameters1.projected_element_count = sizeEstimation;
	parameters1.false_positive_probability = 0.02;
	parameters1.random_seed = 0xA5A5A5A5;

	parameters1.compute_optimal_parameters();
	this->filter1 = new bloom_filter(parameters1);

	// Create second bloom filter to eliminate second-time occuraces
	bloom_parameters parameters2;
	parameters2.projected_element_count = sizeEstimation / 20;
	parameters2.false_positive_probability = 0.02;
	parameters2.random_seed = 0xA5A5A5A5;

	parameters2.compute_optimal_parameters();
	this->filter2 = new bloom_filter(parameters2);
}

void KmerCounter::addKmer(const Kmer kmer) {
	if (!this->filter1->contains(kmer.sequence, kmer.size())) {
		this->filter1->insert(kmer.sequence, kmer.size());
	}
	else if (!this->filter2->contains(kmer.sequence, kmer.size())) {
		this->filter2->insert(kmer.sequence, kmer.size());
	}
	else {
#pragma omp critical
		{
			KmerMap::iterator itr = this->kmerMap.find(kmer);

			if (itr == this->kmerMap.end()) {
				this->kmerMap.insert(KmerItem(kmer, 3));
			}
			else {
				itr->second++;
			}
		}
	}
}

KmerList KmerCounter::getSortedList(size_t listLength) const {
	KmerList sortedKmers;
	for (auto itr = this->kmerMap.begin(); itr != this->kmerMap.end(); itr++) {
		KmerList::iterator position = lower_bound(sortedKmers.begin(), sortedKmers.end(), *itr, [](const KmerItem& first, const KmerItem& second) {
			return first.second > second.second;
		});
		sortedKmers.insert(position, *itr);

		if (sortedKmers.size() > listLength) {
			sortedKmers.pop_back();
		}
	}
	return sortedKmers;
}