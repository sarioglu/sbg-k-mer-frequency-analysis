#pragma once

#include <stdint.h>

#include <unordered_map>
#include <list>

#include "Kmer.h"
#include "bloom_filter.hpp"

typedef std::pair<Kmer, int> KmerItem;
typedef std::unordered_map<Kmer, int> KmerMap;
typedef std::list<KmerItem> KmerList;

class KmerCounter
{
public:
	~KmerCounter();
	KmerCounter(KmerCounter const&) = delete;
	void operator=(KmerCounter const&) = delete;

	static KmerCounter& getInstance();

	void init(const uint64_t);
	void addKmer(const Kmer);
	KmerList getSortedList(size_t) const;

private:
	KmerCounter() {};
	void createFilters(const uint64_t);

	bloom_filter* filter1;
	bloom_filter* filter2;

	KmerMap kmerMap;

};