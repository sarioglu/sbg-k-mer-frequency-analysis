#include <stdlib.h>
#include <stdint.h>
#include <getopt.h>

#include <iostream>
#include <string>

#include "Kmer.h"
#include "FastqReader.h"
#include "KmerCounter.h"
#include "bloom_filter.hpp"

using namespace std;

void processFastqFile(FastqReader&, KmerCounter&, const size_t);
void extractKmers(KmerCounter&, const string, const size_t);
void printTopList(const KmerList&);

int main(int argc, char** argv) {
	int returnValue = EXIT_SUCCESS;

	int option;
	string filename = "";
	int kmerLength = 30, listLength = 25;
	
	while ((option = getopt(argc, argv, "f:k:c:")) != EOF) {
		switch (option) {
		case 'f':
			filename = string(optarg);
			break;
		case 'k':
			kmerLength = atoi(optarg);
			break;
		case 'c':
			listLength = atoi(optarg);
			break;
		default:
			//TODO: Do some error check
			cout << "Usage: " << argv[0] << " -f filename [-k kmerLength] [-c listLength]" << endl;
			returnValue = EXIT_FAILURE;
			break;
		}
	}
	
	if (returnValue != EXIT_FAILURE) {
		if (filename == "") {
			cout << "You have to provide a valid file to process" << endl;
			returnValue = EXIT_FAILURE;
		}
		else {
			cout << "Starting to process file (-f): " << filename << " with parameters:" << endl;
			cout << "kmerLength (-k): " << kmerLength << endl;
			cout << "listLength (-c): " << listLength << endl;
			cout << endl;

			Kmer::setCount(kmerLength);
			FastqReader& fastqReader = FastqReader::getInstance(filename);
			KmerCounter& kmerCounter = KmerCounter::getInstance();

			processFastqFile(fastqReader, kmerCounter, kmerLength);
			printTopList(kmerCounter.getSortedList(listLength));
		}
	}

	return returnValue;
}

void processFastqFile(FastqReader& fastqReader, KmerCounter& kmerCounter, const size_t kmerLength) {
	if (!fastqReader.isOpen()) {
		cout << "Cannot open the provided file" << endl;
	}
	else {
		string sequence = "";
		uint64_t kmerEstimation = 0;

		while ((sequence = fastqReader.getNextSequence()) != "") {
			if (kmerEstimation == 0) {
				const uint64_t filesize = fastqReader.getFileSize();
				const size_t sequenceLength = sequence.length();

				kmerEstimation = (filesize * (sequenceLength - kmerLength)) / (2.5 * sequenceLength);
				kmerCounter.init(kmerEstimation);
			}

			extractKmers(kmerCounter, sequence, kmerLength);
		}
	}
}

void extractKmers(KmerCounter& kmerCounter, const string sequence, const size_t kmerLength) {
	const int lastIndex = sequence.length() - kmerLength;

#pragma omp parallel for
	for (int index = 0; index <= lastIndex; index++) {
		kmerCounter.addKmer(Kmer(sequence.substr(index, kmerLength)));
	}
}

void printTopList(const KmerList& kmerList) {
	if (kmerList.size() == 0) {
		cout << "No result to display!" << endl;
	}
	else {
		cout << "K-MER SEQUENCE" << "\t" << "COUNT" << endl;
		cout << "==============" << "\t" << "=====" << endl;
		cout << endl;

		for (auto itr = kmerList.begin(); itr != kmerList.end(); itr++) {
			cout << itr->first.toString() << "\t" << itr->second << endl;
		}
	}
}