#pragma once

#define _SCL_SECURE_NO_WARNINGS

#include <stdint.h>
#include <string>
#include <math.h>
#include <functional>
#include <algorithm>

#define COMP_RATIO 4

class Kmer {
public:
	uint8_t* sequence;
	static size_t count;

	Kmer(const std::string);
	Kmer(const Kmer&);
	~Kmer();

	size_t size() const;
	std::string toString() const;
	bool operator==(const Kmer&) const;
	size_t hash() const;

	static void setCount(const int);
};

namespace std {
	template<> struct hash<Kmer> {
		size_t operator()(const Kmer & x) const {
			return x.hash();
		}
	};
}
