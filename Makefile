CXX= g++
CXXFLAGS= -std=c++11 -Wall -fopenmp

DEPS= Kmer.h KmerCounter.h FastqReader.h bloom_filter.hpp
SRC= Kmer.cpp FastqReader.cpp KmerCounter.cpp SgbCodingTask.cpp

TARGET= kmerCounter

$(TARGET): $(SRC)
	$(CXX) -o $@ $^ $(CXXFLAGS)

all: $(TARGET)

clean:
	$(RM) $(TARGET)

.PHONY: all clean
