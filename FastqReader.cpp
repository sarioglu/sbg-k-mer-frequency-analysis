#include "FastqReader.h"

FastqReader::FastqReader(std::string filename) {
	if (filename != "") {
		this->file.open(filename);
		if (this->isOpen()) {
			this->file.seekg(0, file.end);
			this->filesize = file.tellg();
			this->file.seekg(0, file.beg);
		}
	}
}

FastqReader::~FastqReader() {
	if (this->file.is_open()) {
		this->file.close();
	}
}

FastqReader& FastqReader::getInstance(std::string filename) {
	static FastqReader instance(filename);

	return instance;
}

std::string FastqReader::getNextSequence() {
	std::string sequence = "";
	if (this->file.good()) {
		//Skip the SEQ_ID line
		this->file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		std::getline(this->file, sequence);

		//Skip the '+' line
		this->file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		//Skip the quality line
		this->file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}

	return sequence;
}

uint64_t FastqReader::getFileSize() const {
	return this->filesize;
}

bool FastqReader::isOpen() const {
	return this->file.is_open();
}