#include "Kmer.h"

size_t Kmer::count = 0;

Kmer::Kmer(const std::string dna) {
	size_t sequenceLength = this->size();

	this->sequence = new uint8_t[sequenceLength];
	std::fill(this->sequence, this->sequence + sequenceLength, 0);
	//memset(this->sequence, 0, sequenceLength);

	for (int i = 0; i < Kmer::count; i++) {
		int index = i / COMP_RATIO;
		int shift = COMP_RATIO - 1 - (i % COMP_RATIO);
		char data = dna[i];

		switch (data) {
		case 'A':
			this->sequence[index] |= (0x00 << (2 * shift));
			break;
		case 'T':
			this->sequence[index] |= (0x01 << (2 * shift));
			break;
		case 'G':
			this->sequence[index] |= (0x02 << (2 * shift));
			break;
		case 'C':
			this->sequence[index] |= (0x03 << (2 * shift));
			break;
		}
	}
}

Kmer::Kmer(const Kmer& other) {
	size_t sequenceLength = this->size();

	this->sequence = new uint8_t[sequenceLength];
	std::copy(other.sequence, other.sequence + sequenceLength, this->sequence);
}

Kmer::~Kmer() {
	delete[] this->sequence;
}

size_t Kmer::size() const {
	return ceil(Kmer::count / (float)COMP_RATIO);
}

std::string Kmer::toString() const {
	std::string data = "";

	for (int i = 0; i < Kmer::count; i++) {
		int index = i / COMP_RATIO;
		int shift = COMP_RATIO - 1 - (i % COMP_RATIO);

		switch ((this->sequence[index] >> (2 * shift)) & 0x03) {
		case 0x00:
			data += 'A';
			break;
		case 0x01:
			data += 'T';
			break;
		case 0x02:
			data += 'G';
			break;
		case 0x03:
			data += 'C';
			break;
		}
	}

	return data;
}

bool Kmer::operator==(const Kmer& other) const {
	size_t sequenceLength = this->size();

	return std::equal(other.sequence, other.sequence + sequenceLength, this->sequence);

	//return other.toString() == this->toString();
}

size_t Kmer::hash() const {
	return std::hash<std::string>()(this->toString());
}

void Kmer::setCount(const int kmerLength) {
	Kmer::count = kmerLength;
}
