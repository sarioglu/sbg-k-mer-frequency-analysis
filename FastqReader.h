#pragma once

#include <stdint.h>
#include <fstream>
#include <string>
#include <limits>

class FastqReader
{
public:
	~FastqReader();
	FastqReader(FastqReader const&) = delete;
	void operator=(FastqReader const&) = delete;

	static FastqReader& getInstance(std::string);

	std::string getNextSequence();
	uint64_t getFileSize() const;
	bool isOpen() const;

private:
	FastqReader(std::string);

	std::ifstream file;
	uint64_t filesize;
};

